#!/bin/bash

# Author: r.giovanardi@advenias.it
# Date: 20210520

set -euf -o pipefail

DB_URI=$1
S3_BUCKET=$2
DATE_NOW=$(date +%Y%m%d_%H%M%S)

# extract DB Host and NAME
DB_HOST=$(echo ${DB_URI} | cut -f2 -d'@' | cut -f1 -d':' )
DB_NAME=$(echo ${DB_URI} | rev | cut -f1 -d'/' | rev)

# check for connectivity

psql -d ${DB_URI} -c 'select version();'

# run backup
time pg_dump ${DB_URI} | gzip -c > /mnt/pg_dump-${DB_NAME}-${DB_HOST}-${DATE_NOW}.sql.gz

# run S3 Copy
time aws s3 cp /mnt/pg_dump-${DB_NAME}-${DB_HOST}-${DATE_NOW}.sql.gz s3://${S3_BUCKET}/postgres_ec2_backups/