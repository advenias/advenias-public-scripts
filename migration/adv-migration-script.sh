#!/bin/bash

# Author: r.giovanardi@advenias.it
# Date: 20210713

set -euf -o pipefail

######################################################
# FUNCTIONS
######################################################

# print pre
function echo_pre() {
  echo "" 
  echo ""
  echo "==[ Advenias Migration Script ]=="
  echo ""
}

# Print help
function print_help() {
  echo "Script for migrating DBs"
  echo ""
  echo "Syntax: adv-migration-script.sh [-h|-s]"
  echo ""
  echo "options:"
  echo "  -h   Print Help"
  echo "  -s   Source DB URI format: postgres://username:password@host:5432/db_name"
  echo "  -d   Destination DB URI format: postgres://username:password@host:5432/db_name"
  echo "  -l   list of tables to be checked"
}

function set_log_file() {
  export LOGFILE=$(pwd)/adv-migration-script-checks.$(date +%Y%m%d-%H%M%S).log
}

function check_for_db_connectivity() {
  # check for connectivity
  DB_URI=$1
  
  echo "INFO - check DB connectivity" | tee -a ${LOGFILE} 2>&1
  psql -d ${DB_URI} -c 'select version();' | tee -a ${LOGFILE} 2>&1
  echo "" | tee -a ${LOGFILE} 2>&1
}

function print_db_size() {
  # print db size
  
  DB_URI=$1
  DB_NAME=$2
  
  echo "INFO - printing DB Size"
  psql -d ${DB_URI} <<SQL  | tee -a ${LOGFILE} 2>&1
SELECT
    pg_database_size ('${DB_NAME}')
    ;  
SQL
}

function print_table_list_size() {
  # print table db size
  
  DB_URI=$1
  FILE_LIST=$2
  
  while IFS= read -r TABLE ; do
    echo "INFO - table ${TABLE} size:" | tee -a ${LOGFILE} 2>&1
    psql -d ${DB_URI} <<SQL | tee -a ${LOGFILE} 2>&1
SELECT pg_total_relation_size('${TABLE}');
SQL
    echo "" | tee -a ${LOGFILE} 2>&1
  done < ${FILE_LIST}
}

function print_table_list_count() {
  # print table db size
  
  DB_URI=$1
  FILE_LIST=$2
  
  while IFS= read -r TABLE ; do
    echo "INFO - table ${TABLE} row count:" | tee -a ${LOGFILE} 2>&1
    psql -d ${DB_URI} <<SQL | tee -a ${LOGFILE} 2>&1
SELECT count(*) from ${TABLE};
SQL
    echo "" | tee -a ${LOGFILE} 2>&1
  done < ${FILE_LIST}
}

function print_sequence_values() {
  # print Sequence values
  
  DB_URI=$1
  
  # getting a sequences list
  psql -d ${DB_URI} <<SQL > tmp.relation.list
SELECT c.relname FROM pg_class c WHERE c.relkind = 'S' order by c.relname asc;
SQL
  
  for REL_RAW in $(cat tmp.relation.list | grep -v 'relname' | grep -v "\-\-\-\-" | grep -v \( | grep -v \)); do
    REL="$(echo -e "${REL_RAW}" | tr -d '[:space:]')"
    echo "INFO - checking sequence ${REL}" | tee -a ${LOGFILE} 2>&1
    psql -d ${DB_URI} <<SQL | tee -a ${LOGFILE} 2>&1
select last_value from ${REL};
SQL
  done
  
  echo "" | tee -a ${LOGFILE} 2>&1
  rm tmp.relation.list
}

function pg_dump_db() {
  # run pgdump db
  # storage dest: /mnt/source/db
  DB_URI=$1
  DB_NAME=$2
  
  time pg_dump ${DB_URI} \
     --format=directory \
     --create \
     --jobs=8 \
     --compress=0 \
     --no-privileges \
     --no-owner \
     --file=${STORAGE_FOLDER}/${DB_NAME}.${DATE_NOW}.dump \
      | tee -a ${LOGFILE} 2>&1
  
  # verify it
  echo "INFO - this is the pg_dump result:"
  ls -l ${STORAGE_FOLDER}/${DB_NAME}.${DATE_NOW}.dump | tee -a ${LOGFILE} 2>&1
  [ -d ${STORAGE_FOLDER}/${DB_NAME}.${DATE_NOW}.dump ] || ( echo "ERROR - pgdump failed" ; exit 1 )
}

function pg_dump_db_plaintext() {
  # run pgdump db
  # storage dest: /mnt/source/db
  DB_URI=$1
  DB_NAME=$2
  
  time pg_dump ${DB_URI} \
     --format=plain \
     --create \
     --no-privileges \
     --no-owner \
     --file=${STORAGE_FOLDER}/${DB_NAME}.${DATE_NOW}.dump \
      | tee -a ${LOGFILE} 2>&1
  
  # verify it
  echo "INFO - this is the pg_dump result:"
  ls -l ${STORAGE_FOLDER}/${DB_NAME}.${DATE_NOW}.dump | tee -a ${LOGFILE} 2>&1
  [ -d ${STORAGE_FOLDER}/${DB_NAME}.${DATE_NOW}.dump ] || ( echo "ERROR - pgdump failed" ; exit 1 )
}

function pg_dump_ddl() {
  # run pgdump db
  # storage dest: /mnt/source/db
  DB_URI=$1
  DB_NAME=$2
  
  time pg_dump ${DB_URI} \
     --schema-only \
     --format=directory \
     --create \
     --jobs=8 \
     --compress=0 \
     --file=${STORAGE_FOLDER}/${DB_NAME}.${DATE_NOW}-ddl.dump \
      | tee -a ${LOGFILE} 2>&1
  
  # verify it
  echo "INFO - this is the pg_dump result:"
  ls -l ${STORAGE_FOLDER}/${DB_NAME}.${DATE_NOW}-ddl.dump | tee -a ${LOGFILE} 2>&1
  [ -d ${STORAGE_FOLDER}/${DB_NAME}.${DATE_NOW}-ddl.dump ] || ( echo "ERROR - pgdump failed" ; exit 1 )
}

function pg_restore_db() {
  # run the restore
  DB_HOST=$1
  DB_NAME=$2
  DB_USERNAME=$3
  
  echo "INFO - running restore on ${DB_HOST} for ${DB_NAME} with ${DB_USERNAME}" | tee -a ${LOGFILE} 2>&1
  echo "INFO - are you sure to continue? y/n" | tee -a ${LOGFILE} 2>&1
  read ANSWER
  if [[ "${ANSWER}" == "y" ]]; then
    #using --dbname postgres just for launchpad not destination, because --create is used
    time pg_restore \
      --host ${DB_HOST} \
      --create \
      --clean \
      --format=directory \
      --jobs 8 \
      --dbname postgres \
      --no-owner \
      --no-privileges \
      --username ${DB_USERNAME} \
      ${STORAGE_FOLDER}/${DB_NAME}.${DATE_NOW}.dump \
        | tee -a ${LOGFILE} 2>&1
  else
    echo "INFO - you choose not to restore" | tee -a ${LOGFILE} 2>&1
  fi
}

function pg_restore_ddl() {
  # run the restore
  DB_HOST=$1
  DB_NAME=$2
  DB_USERNAME=$3
  
  echo "INFO - running DDL restore on ${DB_HOST} for ${DB_NAME} with ${DB_USERNAME}" | tee -a ${LOGFILE} 2>&1
  echo "INFO - are you sure to continue? y/n" | tee -a ${LOGFILE} 2>&1
  read ANSWER
  if [[ "${ANSWER}" == "y" ]]; then
    time pg_restore \
      --create \
      --host ${DB_HOST} \
      --format=directory \
      --jobs 8 \
      --dbname ${DB_NAME} \
      --username ${DB_USERNAME} \
      ${STORAGE_FOLDER}/${DB_NAME}.${DATE_NOW}-ddl.dump \
        | tee -a ${LOGFILE} 2>&1
  else
    echo "INFO - you choose not to restore" | tee -a ${LOGFILE} 2>&1
  fi
}


######################################################
# MAIN
######################################################

echo_pre
set_log_file

while getopts ":hs:l:d:" flag
do
    case "${flag}" in
        h) 
          print_help
          exit 0
          ;;
        s)
          SOURCE_DB_URI=${OPTARG}
          ;;
        d)
          DEST_DB_URI=${OPTARG}
          ;;
        l)
          FILE_LIST=${OPTARG}
          ;;
        *) 
          print_help
          exit 0
          ;;
        #f) fullname=${OPTARG};;
    esac
done

# case no arguments
if [[ $# -eq 0 ]] ; then
    print_help
    exit 0
fi

# hardcoded vars
STORAGE_FOLDER="/mnt/"
DATE_NOW=$(date +%Y%m%d-%H%M%S)

# extract DB Host and NAME
SOURCE_DB_HOST=$(echo ${SOURCE_DB_URI} | cut -f2 -d'@' | cut -f1 -d':' )
SOURCE_DB_NAME=$(echo ${SOURCE_DB_URI} | rev | cut -f1 -d'/' | rev)
SOURCE_DB_USERNAME=$(echo ${SOURCE_DB_URI} | cut -f3 -d'/' | cut -f1 -d':')
echo "INFO - SOURCE DB info:"
echo "INFO - host: ${SOURCE_DB_HOST}"
echo "INFO - db name: ${SOURCE_DB_NAME}"
echo ""


DEST_DB_HOST=$(echo ${DEST_DB_URI} | cut -f2 -d'@' | cut -f1 -d':' )
DEST_DB_NAME=$(echo ${DEST_DB_URI} | rev | cut -f1 -d'/' | rev)
DEST_DB_USERNAME=$(echo ${DEST_DB_URI} | cut -f3 -d'/' | cut -f1 -d':')
echo "INFO - DEST DB info:"
echo "INFO - host: ${DEST_DB_HOST}"
echo "INFO - db name: ${DEST_DB_NAME}"
echo ""


echo "" | tee -a ${LOGFILE} 2>&1
echo "#######################################################################" | tee -a ${LOGFILE} 2>&1
echo "#######################  SOURCE DB Checks  ############################" | tee -a ${LOGFILE} 2>&1
echo "#######################################################################" | tee -a ${LOGFILE} 2>&1
echo "" | tee -a ${LOGFILE} 2>&1
echo "" | tee -a ${LOGFILE} 2>&1
echo "" | tee -a ${LOGFILE} 2>&1

# checking connectivity
check_for_db_connectivity ${SOURCE_DB_URI}

print_db_size ${SOURCE_DB_URI} ${SOURCE_DB_NAME}

print_table_list_size ${SOURCE_DB_URI} ${FILE_LIST}

print_table_list_count ${SOURCE_DB_URI} ${FILE_LIST}

print_sequence_values ${SOURCE_DB_URI}


echo "" | tee -a ${LOGFILE} 2>&1
echo "#######################################################################" | tee -a ${LOGFILE} 2>&1
echo "#####################  DESTINATION DB Checks  #########################" | tee -a ${LOGFILE} 2>&1
echo "#######################################################################" | tee -a ${LOGFILE} 2>&1
echo "" | tee -a ${LOGFILE} 2>&1
echo "" | tee -a ${LOGFILE} 2>&1
echo "" | tee -a ${LOGFILE} 2>&1

# checking connectivity
# check_for_db_connectivity ${DEST_DB_URI}

# actual migration
pg_dump_db ${SOURCE_DB_URI} ${SOURCE_DB_NAME}
pg_restore_db ${DEST_DB_HOST} ${DEST_DB_NAME} ${DEST_DB_USERNAME}

print_db_size ${DEST_DB_URI} ${DEST_DB_NAME}

print_table_list_size ${DEST_DB_URI} ${FILE_LIST}

print_table_list_count ${DEST_DB_URI} ${FILE_LIST}

print_sequence_values ${DEST_DB_URI}
