# Advenias Public Scripts

This repo contains simple maintenance scripts used by Advenias Srl.

## backup
* `adv-pgbak-to-s3.sh`: script that takes a full DB backup and send it to S3


## migration
* `adv-migration-script.sh`: script that given a source db issues pre-checks and pg_dump and given a destination db issues 
pg_restore and final checks for verifying the migration